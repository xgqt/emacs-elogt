# ElogT

Portage Emerge log browser for GNU Emacs.

## About

ElogT displays the Portage logs in a convenient table and allows log file
browsing and deletion.

## License

Copyright (c) 2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
